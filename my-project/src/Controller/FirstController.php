<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FirstController extends AbstractController
{
    /**
     * @Route("/first", name="first")
     */
    public function index()
    {
        return $this->render('first/index.html.twig', [
            'controller_name' => 'FirstController',
        ]);
    }

       /**
     * @Route("/contact", name="contact")
     */
    public function contact()
    {
       return new Response("Contact Me");
    }

    
        /**
     * @Route("/contact_form", name="contact_form")
     */
    public function contactForm()
    {
       return $this->render('contact/contact.html.twig');
    }

    public function about()
    {
            return $this->render('contact/about.html.twig' , [

                'name' => "Gurinder"

            ]);
    }
}
